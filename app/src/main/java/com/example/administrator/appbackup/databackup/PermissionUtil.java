package com.example.administrator.appbackup.databackup;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;

import com.example.administrator.appbackup.Constant;


public abstract class PermissionUtil {

    public static void goToPermissionSettingScreen(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", activity.getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }
//
////    public static boolean isGroupPermissionGranted(Activity activity, String[] permission) {
////        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
////            if (permission.length == 0) return false;
////            for (String s : permission) {
////                if (ActivityCompat.checkSelfPermission(activity, s) != PackageManager.PERMISSION_GRANTED) {
////                    return false;
////                }
////            }
////        }
////        return true;
////    }

    public static boolean isAllPermissionGranted(Activity activity) {
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            String[] permission = Constant.ALL_REQUIRED_PERMISSION;
            if (permission.length == 0) return false;
            for (String s : permission) {
                if (ActivityCompat.checkSelfPermission(activity, s) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
