package com.example.administrator.appbackup.datarestore;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;

import com.example.administrator.appbackup.Constant;
import com.example.administrator.appbackup.modelrestore.RestoreAppModel;

import java.io.File;
import java.util.ArrayList;

public class Utils {
    public static ArrayList<RestoreAppModel> loadBackupAPK(Context ctx) {
        ArrayList<RestoreAppModel> appList = new ArrayList<>();
        File root = new File(Constant.BACKUP_FOLDER);
        Log.d("A", "loadBackupAPK: "+root.listFiles());
        if (root.exists() && root.isDirectory()) {

            for (File f : root.listFiles()) {
                if ( f.length() > 0 && f.getPath().endsWith(".apk") ) {
                    String filePath = f.getPath();
                    PackageInfo pk = ctx.getPackageManager().getPackageArchiveInfo(filePath, PackageManager.GET_ACTIVITIES);
                    if (pk != null) {
                        ApplicationInfo info = pk.applicationInfo;
                        if ( Build.VERSION.SDK_INT >= 8 ) {
                            info.sourceDir = filePath;
                            info.publicSourceDir = filePath;
                        }
                        Drawable icon = info.loadIcon(ctx.getPackageManager());
                        RestoreAppModel app = new RestoreAppModel();
                        app.setIcon(icon);
                        app.setFile(f);
                        app.setPath(filePath);
                        app.setAppname(f.getName());
                        app.setDate(f.lastModified());
                        appList.add(app);
                    }
                }
            }
        }
        return appList;
    }



}
