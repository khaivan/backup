package com.example.administrator.appbackup.adapterbackup;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.fragmentbackup.FragmentApps;
import com.example.administrator.appbackup.modelbackup.BackupAppsModel;

public class ListBackingupAppsAdapter extends RecyclerView.Adapter<ListBackingupAppsAdapter.ViewHolder> {
    float values;
    IGetLisBA iGetLisBA;

    public ListBackingupAppsAdapter(IGetLisBA iGetLisBA) {
        this.iGetLisBA = iGetLisBA;
    }

    @NonNull
    @Override
    public ListBackingupAppsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_list_backup, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListBackingupAppsAdapter.ViewHolder holder, int position) {
        BackupAppsModel items = iGetLisBA.getItemsBackupApps(position);
        holder.imgIcon.setImageDrawable(items.getIcon());
        holder.tvAppName.setText(items.getAppname());
        holder.progressBar.setMax(FragmentApps.selected_item.size());
        holder.progressBar.setProgress(iGetLisBA.getProgesApps());
        values = ((float) (holder.progressBar.getProgress()) / (float) (holder.progressBar.getMax())) * 100;
        int i = (int) values;
        holder.tvPercent.setText(i + "%");

    }

    @Override
    public int getItemCount() {
        return iGetLisBA.getCountListBuApps();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgIcon;
        private TextView tvPercent, tvAppName;
        private ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.img_icon);
            tvPercent = itemView.findViewById(R.id.tv_percent);
            tvAppName = itemView.findViewById(R.id.tv_name_app);
            progressBar = itemView.findViewById(R.id.progess);
//            progressBar.
        }
    }

    public interface IGetLisBA {
        int getCountListBuApps();

        int getProgesApps();

        BackupAppsModel getItemsBackupApps(int position);


    }
}
