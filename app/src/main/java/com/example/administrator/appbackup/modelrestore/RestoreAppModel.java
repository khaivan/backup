package com.example.administrator.appbackup.modelrestore;

import android.graphics.drawable.Drawable;

import java.io.File;

public class RestoreAppModel {
    private String appname;
    private long    date;
    private long app_memory;
    private String packageName;
    private Drawable icon;
    private int percent;
    private File file;

    private boolean isCheck=false;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    private String path;
    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
        setApp_memory(this.file.length());
    }
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getApp_memory() {
        return app_memory;
    }

    public void setApp_memory(long app_memory) {
        this.app_memory = app_memory;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
