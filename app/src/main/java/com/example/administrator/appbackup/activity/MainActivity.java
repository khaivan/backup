package com.example.administrator.appbackup.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.databackup.PermissionUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout layoutBackup, layoutRestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initsView();
    }

    private void initsView() {
        layoutBackup = findViewById(R.id.line_backup);
        layoutRestore = findViewById(R.id.line1_restore);
        layoutBackup.setOnClickListener(this);
        layoutRestore.setOnClickListener(this);
    }

    @Override
    protected void onResume() {

        if (!PermissionUtil.isAllPermissionGranted(this)) {
            showDialogPermission();
        }
        super.onResume();
    }

    private void showDialogPermission() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_permission));
        builder.setMessage(getString(R.string.dialog_content_permission));
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(getApplicationContext())) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_SMS,
                                Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG,
                                Manifest.permission.WRITE_CALL_LOG, Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.SEND_SMS}, 2909);
                    }
                }
            }
        });
        builder.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.line_backup:
                Intent iBackup = new Intent(MainActivity.this, BackupActivity.class);
                startActivity(iBackup);
                break;

//            case R.id.line1_restore:
//                Intent iRestore = new Intent(MainActivity.this, RestoreActivity.class);
//                startActivity(iRestore);
//                break;
        }

    }
}
