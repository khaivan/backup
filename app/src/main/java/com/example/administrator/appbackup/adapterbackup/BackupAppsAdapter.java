package com.example.administrator.appbackup.adapterbackup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.appbackup.Constant;
import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.modelbackup.BackupAppsModel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BackupAppsAdapter extends RecyclerView.Adapter<BackupAppsAdapter.ViewHolder> {
    private IGetItems iGet;
    private Context context;
    private List<Integer> listCheckboxs;
    private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
    private List<Integer> index = new ArrayList<>();

    public BackupAppsAdapter(IGetItems iGet, Context context) {
        this.iGet = iGet;
        this.context = context;
    }


    @NonNull
    @Override
    public BackupAppsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.app, parent, false);
        listCheckboxs = new ArrayList<>();
        checkBackedUp();

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final BackupAppsAdapter.ViewHolder holder, final int position) {
        final BackupAppsModel appInfo = iGet.getItems(position);
        holder.tvNameApp.setText(appInfo.getAppname());
        holder.imgIcon.setImageDrawable(appInfo.getIcon());
        holder.tvSize.setText(Formatter.formatFileSize(context, appInfo.getApp_memory()));
        holder.tvDate.setText(fmt.format(appInfo.getDate()));
        holder.checkBox.setChecked(false);

        if (index.size()!=0) {
            for (Integer i : index
                    ) {
                if (i == position) {
                    holder.imgSave.setVisibility(View.VISIBLE);
                    holder.checkBox.setVisibility(View.INVISIBLE);
                    holder.itemView.setOnClickListener(null);
                    appInfo.setHide(false);
                    return;
                } else {
                    holder.imgSave.setVisibility(View.INVISIBLE);
                    holder.checkBox.setVisibility(View.VISIBLE);
                    appInfo.setHide(true);
                }
            }
        }
        holder.checkBox.setChecked(appInfo.isChecked);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iGet.onClickItems(position);
                for (Integer p : listCheckboxs
                        ) {
                    if (p == position) {
                        holder.checkBox.setChecked(false);
                        listCheckboxs.remove(p);
                        return;
                    }
                    holder.checkBox.setChecked(true);
                    listCheckboxs.add(position);


                }
                if (appInfo.getChecked()) {
                    holder.checkBox.setChecked(false);
                    appInfo.setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                    appInfo.setChecked(true);

                }


            }


        });
    }

    public void checkBackedUp() {
        if (iGet.getCount()!=0){
            for (int i = 0; i < iGet.getCount(); i++) {
                String filename = iGet.getItems(i).getAppname() + "_" + iGet.getItems(i).getVersionName() + ".apk";
                File outputFile = new File(Constant.BACKUP_FOLDER);
                File apk = new File(outputFile.getPath() + "/" + filename);
                if (apk.exists()) {
                    index.add(i);
//                Log.d("exitst", "checkBackedUp: " + index);
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        return iGet.getCount();
    }

    final class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgIcon;
        private TextView tvNameApp, tvSize, tvDate;
        private CheckBox checkBox;
        private ImageView imgSave;

        public ViewHolder(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.img_icon);
            tvNameApp = itemView.findViewById(R.id.tv_name_app);
            tvSize = itemView.findViewById(R.id.tv_size);
            tvDate = itemView.findViewById(R.id.tv_date);
            checkBox = itemView.findViewById(R.id.checkbox);
            imgSave = itemView.findViewById(R.id.img_save);
            this.setIsRecyclable(false);


        }

    }

    public interface IGetItems {
        int getCount();

        BackupAppsModel getItems(int position);

        void onClickItems(int position);
    }


}
