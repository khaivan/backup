package com.example.administrator.appbackup.fragmentrestore;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;

import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.adapterrestore.RestoreAppAdapter;
import com.example.administrator.appbackup.datarestore.Utils;
import com.example.administrator.appbackup.modelrestore.RestoreAppModel;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class FragmentAppRestore extends Fragment implements RestoreAppAdapter.IGetItems {
    private RecyclerView rcView;
    private RestoreAppAdapter restoreAdapter;
    private static List<RestoreAppModel> ress = new ArrayList<>();
    public static ArrayList<RestoreAppModel> apps;
    private Button btnRestore;
    private List<RestoreAppModel> result_list;
    private Button btnFaded;

    public static ArrayList<RestoreAppModel> selected_item;
    private ItemFilter mFilter = new ItemFilter();
    private String query;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backup, container, false);
        rcView = view.findViewById(R.id.rc_view);
        initsView(view);
        initsAdapter();
        ress = Utils.loadBackupAPK(getActivity());
        selected_item = new ArrayList<>();
        btnRestore.setText("Restore");
        return view;
    }

    private void initsAdapter() {
        restoreAdapter = new RestoreAppAdapter(this, getActivity());
        rcView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcView.setAdapter(restoreAdapter);
    }

    private void initsView(View view) {
        btnRestore = view.findViewById(R.id.btn_backup);
        btnFaded = view.findViewById(R.id.frame);
        btnRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_item.size() == 0) {
                    return;
                }
                restoreApkFiles(selected_item);
            }
        });
    }
    @Override
    public int getCount() {
        if (ress == null)
            return 0;
        if (result_list != null&&result_list.size()!=0) {
            return result_list.size();
        } else {
            return ress.size();

        }
    }

    @Override
    public RestoreAppModel getItems(int position) {
        if (result_list != null&&result_list.size()!=0) {
            return result_list.get(position);
        } else {
            return ress.get(position);

        }
    }

    @Override
    public void onClickItems(int position) {
        for (RestoreAppModel restoreAppModel : selected_item
                ) {
            if (restoreAppModel.equals(ress.get(position))) {
                selected_item.remove(restoreAppModel);
                if (selected_item.size() == 0) {
                    btnFaded.setVisibility(View.VISIBLE);
                }
                return;
            }
        }
        selected_item.add(ress.get(position));
        if (selected_item != null) {
            btnFaded.setVisibility(View.INVISIBLE);
//            return;
        }

    }

    @Override
    public void onResume() {

        btnFaded.setVisibility(View.VISIBLE);
        initsAdapter();
        super.onResume();
    }

    public void restoreApkFiles(List<RestoreAppModel> apklist) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        for (RestoreAppModel restr : apklist) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.setDataAndType(Uri.fromFile(restr.getFile()), "application/vnd.android.package-archive");
            startActivity(intent);
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            btnFaded.setVisibility(View.VISIBLE);
            ress = Utils.loadBackupAPK(getActivity());

            query = constraint.toString().toLowerCase();
            result_list = new ArrayList<>(ress.size());
            FilterResults results = new FilterResults();
            if (query.equals("")) {
                ress = Utils.loadBackupAPK(getActivity());
            }
            result_list = new ArrayList<>(ress.size());
            for (RestoreAppModel app : ress
                    ) {
                if (app.getAppname().toLowerCase().contains(query)) {
                    result_list.add(app);
                }

            }
            results.values = result_list;
            results.count = result_list.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ress = (ArrayList<RestoreAppModel>) results.values;
            restoreAdapter.notifyDataSetChanged();
        }

    }

}

