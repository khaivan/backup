package com.example.administrator.appbackup.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.administrator.appbackup.Constant;
import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.adapterbackup.ListBackingupAppsAdapter;
import com.example.administrator.appbackup.fragmentbackup.FragmentApps;
import com.example.administrator.appbackup.modelbackup.BackupAppsModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ListBackupActivity extends AppCompatActivity implements ListBackingupAppsAdapter.IGetLisBA {
    private RecyclerView rcView;
    private ProgressBar progressBar;
    private int progesApps = 0;
    private int progesPersonal = 0;
    private File appdir, cntctdir, smsdir;
    private FileOutputStream ostrm, log;
    private Calendar cal;
    private Button btnBackup;
    private String fname, logmsg;
    private Cursor dataC, rawC;
    private int count;


    private ListBackingupAppsAdapter listBackupAdapter;
    int i = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_backup);
        rcView = findViewById(R.id.rc_view_list_backup);
        appdir = new File(Constant.BACKUP_FOLDER);
        Intent intent = getIntent();
        int C = intent.getIntExtra("A", 0);

        switch (C) {
            case Constant.APPS:
                new BackupApps(FragmentApps.selected_item).execute();
                initsAdapterApps();
                break;
        }


    }

    public void initsAdapterApps() {
        rcView.setLayoutManager(new LinearLayoutManager(this));
        listBackupAdapter = new ListBackingupAppsAdapter(this);
        rcView.setAdapter(listBackupAdapter);
    }


    @Override
    public int getCountListBuApps() {
        if (FragmentApps.selected_item == null) {
            return 0;
        }
        return FragmentApps.selected_item.size();
    }

    @Override
    public int getProgesApps() {
        return progesApps;
    }

    @Override
    public BackupAppsModel getItemsBackupApps(int position) {
        return FragmentApps.selected_item.get(position);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }




    private class BackupApps extends AsyncTask<Void, Integer, File> {

        //        private ProgressDialog progress;
        private List<BackupAppsModel> selected_app;

        public BackupApps(List<BackupAppsModel> selected_app) {
            this.selected_app = selected_app;
        }

        @Override
        protected void onPreExecute() {
            Log.d("222", "onPreExecute: ");
        }

        @Override
        protected File doInBackground(Void... params) {
            int i = 0;
            File outputFile = null;
            while (selected_app.size() > i) {
                String filename = selected_app.get(i).getAppname() + "_" + selected_app.get(i).getVersionName() + ".apk";
                outputFile = new File(Constant.BACKUP_FOLDER);
                if (!outputFile.exists()) {
                    outputFile.mkdirs();
                }
                File apk = new File(outputFile.getPath() + "/" + filename);
                try {
                    apk.createNewFile();
                    InputStream in = new FileInputStream(selected_app.get(i).getFile());
                    OutputStream out = new FileOutputStream(apk);
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
                i++;
            }
            return outputFile;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progesApps = values[0];
            listBackupAdapter.notifyDataSetChanged();
//            initsAdapterApps();
        }

        @Override
        protected void onPostExecute(File result) {
            progesApps = FragmentApps.selected_item.size();
            new CountDownTimer(2000, 1000) {
                @Override
                public void onTick(long l) {
                }

                @Override
                public void onFinish() {
                    onBackPressed();
                    FragmentApps.selected_item = new ArrayList<>();
                }
            }.start();
            Toast.makeText(ListBackupActivity.this, "App backup Success", Toast.LENGTH_LONG).show();
        }

    }
}
