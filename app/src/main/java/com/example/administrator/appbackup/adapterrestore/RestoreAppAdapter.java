package com.example.administrator.appbackup.adapterrestore;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.modelrestore.RestoreAppModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class RestoreAppAdapter extends RecyclerView.Adapter<RestoreAppAdapter.ViewHolder> {
    private IGetItems iGet;
    private Context context;
    private List<Integer> listCheckboxs = new ArrayList<>();
    private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");

    public RestoreAppAdapter(IGetItems iGet, Context context) {
        this.iGet = iGet;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.app,parent,false);
        return  new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final RestoreAppModel appInfo = iGet.getItems(position);
        holder.tvNameApp.setText(appInfo.getAppname());
        holder.imgIcon.setImageDrawable(appInfo.getIcon());

        holder.tvSize.setText(Formatter.formatFileSize(context,appInfo.getApp_memory()));
        holder.tvDate.setText(fmt.format(appInfo.getDate()));
        holder.checkBox.setChecked(appInfo.isCheck());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iGet.onClickItems(position);
                for (Integer p: listCheckboxs){
                    if (p == position){
                        holder.checkBox.setChecked(false);
                        listCheckboxs.remove(p);
                        return;
                    }
                    holder.checkBox.setChecked(true);
                    listCheckboxs.add(position);
                }
                if (appInfo.isCheck()) {
                    holder.checkBox.setChecked(false);
                    appInfo.setCheck(false);
                } else {
                    holder.checkBox.setChecked(true);
                    appInfo.setCheck(true);

                }

            }
        });



    }

    @Override
    public int getItemCount() {
        return iGet.getCount();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgIcon;
        private TextView tvNameApp,tvSize,tvDate;
        private CheckBox checkBox;
        public ViewHolder(View itemView) {
            super(itemView);
            tvNameApp = itemView.findViewById(R.id.tv_name_app);
            tvSize = itemView.findViewById(R.id.tv_size);
            tvDate = itemView.findViewById(R.id.tv_date);
            imgIcon = itemView.findViewById(R.id.img_icon);
            checkBox = itemView.findViewById(R.id.checkbox);

        }
    }
    public interface IGetItems{
        int getCount();
        RestoreAppModel getItems(int position);
        void onClickItems(int position);
    }
}
