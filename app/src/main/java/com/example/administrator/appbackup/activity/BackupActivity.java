package com.example.administrator.appbackup.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.databackup.PermissionUtil;
import com.example.administrator.appbackup.fragmentbackup.FragmentApps;
import com.example.administrator.appbackup.fragmentrestore.FragmentAppRestore;

import java.util.ArrayList;

public class BackupActivity extends AppCompatActivity {
    private static final String TAG = "TAG";
    private TextView tvCount;
    private Button btnFilt;
    private FragmentApps fragmentBackup;
    private FragmentAppRestore fragmentRestore;
    private SearchView search;
    private Toolbar toolbarMain;
    private ActionBar actionBar;
    private int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);
        inisFragmentBackup();

        initToolbar();
        final TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Backup"));
        tabLayout.addTab(tabLayout.newTab().setText("Restore"));
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal), getResources().getColor(R.color.select));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                i = tabLayout.getSelectedTabPosition();
                switch (i) {
                    case 0:
                        inisFragmentBackup();
                        if (search!=null){
                            search.setQueryHint(getString(R.string.hint_backup_search));
                        }
                        break;
                    case 1:
                        initsFragmentRestore();
                        if (search!=null){
                            search.setQueryHint(getString(R.string.hint_restore_search));
                        }
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
//
    }

    private void showDialogPermission() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_permission));
        builder.setMessage(getString(R.string.dialog_content_permission));
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(getApplicationContext())) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                               }, 2909);
                    }
                }
            }
        });
        builder.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }
    private void initsFragmentRestore() {
         fragmentRestore = new FragmentAppRestore();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, fragmentRestore, FragmentAppRestore.class.getName());
        transaction.commit();
    }
    private void inisFragmentBackup() {
        fragmentBackup = new FragmentApps();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, fragmentBackup, FragmentApps.class.getName());
        transaction.commit();
    }

    private void initToolbar() {
        actionBar = getSupportActionBar();
        toolbarMain = findViewById(R.id.toolbar);
        setSupportActionBar(toolbarMain);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
//        btnFilt = (Button) menu.findItem(R.id.action_filt).getActionView();
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, "onQueryTextChange: ");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                switch (i) {
                    case 0:
                        fragmentBackup.getFilter().filter(s);
                        FragmentApps.selected_item = new ArrayList<>();
                        search.setQueryHint(getString(R.string.hint_backup_search));
                        break;
                    case 1:
                        search.setQueryHint(getString(R.string.hint_restore_search));
                        fragmentRestore.getFilter().filter(s);
                        FragmentAppRestore.selected_item = new ArrayList<>();
                        break;
                    default:
                        break;
                }

                return true;
            }
        });
        search.onActionViewCollapsed();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search: {
                supportInvalidateOptionsMenu();
                return true;
            }
//            case R.id.action_filt:
//                Toast.makeText(this, "action_filt", Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        switch (i) {
            case 0:
                inisFragmentBackup();
                break;
            case 1:
                initsFragmentRestore();
                break;
            default:
                break;
        }

        if (!PermissionUtil.isAllPermissionGranted(this)) {
            showDialogPermission();
        }
        super.onResume();
    }


}
