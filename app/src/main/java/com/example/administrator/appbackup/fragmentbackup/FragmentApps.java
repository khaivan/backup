package com.example.administrator.appbackup.fragmentbackup;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.administrator.appbackup.Constant;
import com.example.administrator.appbackup.activity.ListBackupActivity;
import com.example.administrator.appbackup.R;
import com.example.administrator.appbackup.adapterbackup.BackupAppsAdapter;
import com.example.administrator.appbackup.modelbackup.BackupAppsModel;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FragmentApps extends android.support.v4.app.Fragment implements BackupAppsAdapter.IGetItems {
    private RecyclerView rcView;
    public BackupAppsAdapter backupAdapter;
    public static ArrayList<BackupAppsModel> apps;
    private ArrayList<BackupAppsModel> res;
    private Button btnBackup;
    private Button btnFaded;
    public static ArrayList<BackupAppsModel> selected_item;
    private ItemFilter mFilter = new ItemFilter();
    private List<BackupAppsModel> result_list;
    private String query;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backup, container, false);
        rcView = view.findViewById(R.id.rc_view);
        initsView(view);
        selected_item = new ArrayList<>();
        getPackages();
        initsAdapter();
        return view;
    }


    private void initsAdapter() {
        backupAdapter = new BackupAppsAdapter(this, getActivity());
        rcView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcView.setAdapter(backupAdapter);


    }

    public ArrayList<BackupAppsModel> getPackages() {
        apps = getInstalledApps(false); /* false = no system packages */
        return apps;
    }

    private void initsView(View view) {
        btnBackup = view.findViewById(R.id.btn_backup);
        btnFaded = view.findViewById(R.id.frame);

        btnBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_item.size() == 0) {
                    return;
                }
                Intent intent = new Intent(getActivity(), ListBackupActivity.class);
                intent.putExtra("A", Constant.APPS);
                startActivity(intent);
            }
        });
    }

    public ArrayList<BackupAppsModel> getInstalledApps(boolean getSysPackages) {
        res = new ArrayList<>();
        List<PackageInfo> packs = getActivity().getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packs.size(); i++) {
            PackageInfo p = packs.get(i);
            if ((p.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                BackupAppsModel newInfo = new BackupAppsModel();
                newInfo.setAppname(p.applicationInfo.loadLabel(getActivity().getPackageManager()).toString());
                newInfo.setDate(new Date(p.lastUpdateTime));
                newInfo.setPackageName(p.packageName);
                newInfo.setVersionName(p.versionName);
                newInfo.setFile(new File(p.applicationInfo.publicSourceDir));
                newInfo.setIcon(p.applicationInfo.loadIcon(getActivity().getPackageManager()));
                res.add(newInfo);

            }
        }

        return res;
    }

    @Override
    public int getCount() {
        if (apps == null)
            return 0;
        if (result_list!=null&&result_list.size()!=0) {
            return result_list.size();
        } else {
            return apps.size();

        }
    }



    @Override
    public BackupAppsModel getItems(int position) {
        try {

            if (result_list != null && result_list.size() != 0) {
                return result_list.get(position);
            } else {
                return apps.get(position);

            }
        } catch (IndexOutOfBoundsException e) {
            Toast.makeText(getActivity(), "Error:Could not find backup file!",
                    Toast.LENGTH_LONG).show();
        }


        return null;
    }

    @Override
    public void onClickItems(int position) {

        for (BackupAppsModel backupModel : selected_item
                ) {
            if (backupModel.equals(apps.get(position))) {
                selected_item.remove(backupModel);
                if (selected_item.size() == 0) {
                    btnFaded.setVisibility(View.VISIBLE);

                }
                return;
            }
        }
        selected_item.add(apps.get(position));
        if (selected_item != null) {
            btnFaded.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResume() {
        btnFaded.setVisibility(View.VISIBLE);
        initsAdapter();
        super.onResume();
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            getPackages();
            btnFaded.setVisibility(View.VISIBLE);
            query = constraint.toString().toLowerCase();
            result_list = new ArrayList<>(apps.size());
            FilterResults results = new FilterResults();
            if (query.equals("")) {
                getPackages();
            }
            result_list = new ArrayList<>(apps.size());
            for (BackupAppsModel app : apps
                    ) {
                if (app.getAppname().toLowerCase().contains(query)) {
                    result_list.add(app);
                }

            }
            results.values = result_list;
            results.count = result_list.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            apps = (ArrayList<BackupAppsModel>) results.values;
            backupAdapter.notifyDataSetChanged();
        }

    }

}
